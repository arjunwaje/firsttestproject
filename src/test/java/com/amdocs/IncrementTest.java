package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
	
	@Test
	public void testdecreasecounter01(){
		int i = new Increment().decreasecounter(0);
		assertEquals("Increament 0", 0, i);

		int j = new Increment().decreasecounter(1);
		 assertEquals("Increament 1", 1, j);
		
		int k = new Increment().decreasecounter(2);
                 assertEquals("Increament 2", 2, k);
	}

}
